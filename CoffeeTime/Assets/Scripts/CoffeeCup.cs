﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class CoffeeCup : MonoBehaviour
    {
        //String Wert für den Szenenamen
        [SerializeField]
        private string ScenenameForMenu;

        //Wenn der Spieler den Collider berührt, soll er ins Menu kommen
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                SceneManager.LoadScene(ScenenameForMenu);
            }
        }
    }
}
