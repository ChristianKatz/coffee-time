﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
   public class CoinControll : MonoBehaviour
   {       
    //GameObjekt, dass die Kaffeebohnen nach einer Berührung des Spielers zerstört
    [SerializeField]
    private GameObject Coffeebean;

    //Rufe den Skript "UIControll" auf, um den Score höher zu setzen 
    private UIControll uIControll;

    void Start()
    {     
        //Der Skript wird aufgreufen
        uIControll = FindObjectOfType<UIControll>();
    }

    //Bei einer Berührung des Spielers wird der Score um 1 erhöht und die Kaffeebohne zerstört
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            uIControll.CurrentScore += 1;
            Destroy(Coffeebean);                     
        }
    }

   }
}
