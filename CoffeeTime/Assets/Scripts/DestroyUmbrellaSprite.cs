﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class DestroyUmbrellaSprite : MonoBehaviour
    {
        //Um den Regenschirm Sprite zu zerstören, wenn der Spieler es berührt, brauche ich das GameObjekt
        [SerializeField]
        private GameObject UmbrellaObject;

        //Dieser Bool Wert schickt es weiter zu den "Umbrella" Skript, damit der Regenschirm aktiviert werden kann
        private bool destroyedSprite;
        public bool DestroyedSprite
        {
            get
            {
                return destroyedSprite;
            }
            set
            {
                destroyedSprite = value;
            }
        }

        void Start()
        {
            //Da der Bool Wert auf Default Einstellungen immer true ist, wird er beim Start auf false gesetzt
            DestroyedSprite = false;
        }

        //Trigger setzten, um den Sprite bei einer Berührung vom Spieler zu zerstören und somit den Bool Wert auf true zu setzten
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                DestroyedSprite = true;
                Destroy(UmbrellaObject);
            }
        }
    }
}
 