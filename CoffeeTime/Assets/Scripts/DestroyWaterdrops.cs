﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class DestroyWaterdrops : MonoBehaviour
    {
        //Um die Regentropfen auf dem Regenschirm zerstören zu lassen, brauche ich zugang zum Regenschirmes
        private Umbrella destroyUmbrella;

        void Start()
        {
            //Skript wird aufgerufen
            destroyUmbrella = FindObjectOfType<Umbrella>();
        }

        //Beim Start des Spieles wird der Regeschirm ausgeschaltet und wird durch einen anderen Skript durch die F Taste aktiviert
        //Dann wird die Coroutine gestartet, die den Cooldown des aktivierten Regenschirms zählt
        private void OnEnable()
        {
            StartCoroutine(DestroyUmbrella());
        }

        //Trigger setzten, um die Wassertropfen zu zerstören, wenn sie den Regenschirm berühren
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Waterdrops"))
            {
                Destroy(other.gameObject);
            }
        }

        //Ist der Cooldown, wie lange der Regenschirm aktiviert ist
        //Danach wird er zerstört
        IEnumerator DestroyUmbrella()
        {
            yield return new WaitForSeconds(10f);
            destroyUmbrella.Shield.SetActive(false);
        }
    }
}
