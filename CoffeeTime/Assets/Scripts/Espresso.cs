﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Espresso : MonoBehaviour
    {
        //Um den Espresso zu zerstören, brauche ich das GameObjekt
        [SerializeField]
        private GameObject Coffee;

        //Um die Bewegung des Spielers zu verändern, brauche ich den "PlayerMovement" Skript
        private PlayerMovement Player;

        //Deaktivierung des Sprite Renderers und des Colliders nach dem Aufsammeln 
        //Weil das Objekt soll durch die Coroutine nicht direkt zerstört werden und andernseits darf der Spieler es nicht merfach aufsammeln
        private SpriteRenderer EspressoSprite;
        private BoxCollider2D EspressoCollider;

        //Gesichter der Spielfigur, die sich nach dem Aufsammeln des Espressos ändern
        [SerializeField]
        private GameObject HighFace;
        [SerializeField]
        private GameObject NormalFace;

        void Start()
        {
            //Die Skripte und Komponenten aufrufen        
            Player = FindObjectOfType<PlayerMovement>();
            EspressoSprite = GetComponent<SpriteRenderer>();
            EspressoCollider = GetComponent<BoxCollider2D>();

            //HighFace erstmal ausschalten
            HighFace.SetActive(false);
        }
        //Wenn der Spieler den Collider berührt wird die Umgebung langsamer, der Spieler behält dabei seine Geschwindigkeit
        //Collider und Sprite Renderer werden deaktiviert und die Coroutine wird gestartet 
        //HighFace wird aktiviert und das NormalFace wird deaktiviert
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                Time.timeScale = 0.5f;
                Time.fixedDeltaTime = Time.timeScale * 0.02f;
                Player.runSpeed = 22;
                Player.jumpForce = 22;
                Player.RB.gravityScale = 7;
                EspressoSprite.enabled = false;
                EspressoCollider.enabled = false;
                HighFace.SetActive(true);
                NormalFace.SetActive(false);
                StartCoroutine(SlowMotion());
            }
        }

        //Cooldown für die SlowMotion, danach ist die Umgebung wieder in der Standard Geschwindigkeit
        //Der Espresso wird aus dem Spiel entfernt
        //NormalFace wird aktiviert und HighFace deaktiviert
        IEnumerator SlowMotion()
        {
            yield return new WaitForSeconds(5f);
            Time.timeScale = 1;
            Player.runSpeed = 10;
            Player.jumpForce = 12;
            Player.RB.gravityScale = 2;
            HighFace.SetActive(false);
            NormalFace.SetActive(true);
            Destroy(Coffee);
        }
    }
}
