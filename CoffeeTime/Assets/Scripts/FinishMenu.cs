﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace CoATwoLA
{
    public class FinishMenu : MonoBehaviour
    {
        //String Wert für den Szenenamen
        [SerializeField]
        private string ScenenameForMenu;

        //String Wert für die Anzeige der Kaffeebohnen
        [SerializeField]
        private TextMeshProUGUI Coffeebeans;

        //String Wert für die Anzeige der Zeit
        [SerializeField]
        private TextMeshProUGUI Time;

        //Int und Float Wert, die die Kaffeebohnen und die Zeit zählen
        private int CoffeebeansScore;
        private float TimeScore;

        void Start()
        {
            //Die gespeicherten Variableln für den Highscore aufrufen
            CoffeebeansScore = PlayerPrefs.GetInt("Coffeebeans", 0);
            TimeScore = PlayerPrefs.GetFloat("Time", 0);

            //Cursor soll ebewegbar und sichtbar sein, um das Menü bedienen zu können
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void Update()
        {
            //Den Highscore von den Kaffeebohnen und der Zeit anzeigen lassen
            Coffeebeans.text = string.Format("Coffeebeans: {0:0}", CoffeebeansScore);
            Time.text = string.Format("Time: {0:0}", TimeScore);
        }

        //Methode für ein Button, der zurück ins Menü führt
        public void BackToTheMenu()
        {
            SceneManager.LoadScene(ScenenameForMenu);
        }

        //Methode für einen Button, der das Level nochmal lädt
        public void Again()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }

        //Methode für ein Button, der das nächste Level lädt
        public void NextLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

    }
}
