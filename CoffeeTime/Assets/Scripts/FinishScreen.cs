﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class FinishScreen : UIControll
    {
        //Text, der für das Anzeigen des Highscores von den Kaffeebohnen nötig ist
        [SerializeField]
        private TextMeshProUGUI Coffeebeans;

        //Text, der für das Anzeigen des Highscores von der Zeit nötig ist
        [SerializeField]
        private TextMeshProUGUI Time;

        //Int Wert, der den Kaffeebohnen Score zählt
        private int CoffeebeansScore;

        //Float Wert, der die Zeit zählt
        private float TimeScore;

        //String Wert für den Szenennamen
        [SerializeField]
        private string ScenenameForMenu;


        void Start()
        {
            //Aufrufen des gepeichterten Highscores von Kaffeebohnen und Zeit
            CoffeebeansScore = PlayerPrefs.GetInt("Coffeebeans", 0);
            TimeScore = PlayerPrefs.GetFloat("Time", 0);

            //Cursor ist entlockt und sichtbar, damit das Menü bedient werden kann
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void Update()
        {
            //Zeigt den Highscore
            Coffeebeans.text = string.Format("Coffeebeans: {0:0}", CoffeebeansScore);
            Time.text = string.Format("Time: {0:0}", TimeScore);
        }

        //Methode für einen Button, der den gespeicherten Score löscht
        public void ResetScore()
        {
            PlayerPrefs.DeleteKey("Coffeebeans");
            PlayerPrefs.DeleteKey("Time");
        }

        //Methode für einen Button, der zurück ins Menü führt
        public void BackToTheMenu()
        {
            SceneManager.LoadScene(ScenenameForMenu);
        }
    }
}

