﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Hairdryer : MonoBehaviour
    {
        //GameObjekt, um nach dem Aufsammeln des Föhnes es zu zerstören
        [SerializeField]
        private GameObject Lifebringer;

        //Ich brauche den Skript, um die Leben des Spielers zu beeinflussen
        private PlayerMovement Player;

        void Start()
        {
            //Den Skript aufrufen
            Player = FindObjectOfType<PlayerMovement>();
        }

        //Trigger setzen, um den Spieler die Leben zu erhöhen, wenn er den Föhn berührt
        //Danach wird das GameObject zerstört
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Player.Health++;
                Destroy(Lifebringer);
            }
        }
    }
}  
