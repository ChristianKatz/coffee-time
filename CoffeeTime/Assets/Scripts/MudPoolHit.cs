﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class MudPoolHit : MonoBehaviour
    {
        //Brauche den Skript, damit der Spieler Leben abgezogen wird
        private PlayerMovement Player;

        //Brauche die Pfütze als GameObject, um es nach der Berührung des Spielers zu zerstören
        [SerializeField]
        private GameObject MudPool;

        void Start()
        {
            // Skript wird aufgerufen
            Player = FindObjectOfType<PlayerMovement>();
        }

        //Trigger setzen, damit der Spieler schaden bekommt, wenn er die Pfütze berührt
        //Dabei werden ihn 1 Leben von 4 abgezogen, danach wird die Pfütze zerstört
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Player.Health--;
                Destroy(MudPool);
            }
        }
    }
}