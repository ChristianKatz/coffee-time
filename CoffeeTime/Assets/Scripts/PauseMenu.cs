﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class PauseMenu : MonoBehaviour
    {
        //GameObjekt für das Menü, damit es ein- und ausgeschaltet werden kann
        [SerializeField]
        private GameObject Pause;

        //String Wert für den Szenenamen
        [SerializeField]
        private string ScenenameForMenu;

        void Start()
        {
            //Wenn das Spiel gestartet wird soll das Menü ausgeschaltet sein
            Pause.SetActive(false);
        }

        void Update()
        {
            //Wenn der Spieler das Menü öffnen will, soll er die Escape Taste drücken
            //Dabei wird der Cursor aktiviert und bewegbar
            //Weiterhin soll die Zeit stoppen, damit der Spieler kein Schaden bekommt und die Zeit nicht verfälscht wird
            if (Input.GetKey(KeyCode.Escape))
            {
                Pause.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                Time.timeScale = 0;
            }
        }

        //Methode für ein Button, der zurück ins Menu führt
        //Wenn der Button gedrückt wird, soll die Zeit wieder aktiviert werden
        public void BackToTheMainMenu()
        {
            SceneManager.LoadScene(ScenenameForMenu);
            Time.timeScale = 1;
        }

        //Methode für ein Button, der zurück in Spiel führt
        //Wenn der Button gedrückt wird, soll die Zeit wieder aktiviert werden
        public void Resume()
        {
            Pause.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
        }

        //Methode für ein Button, mit dem der Spieler das Spiel verlassen kann
        //Wenn der Button gedrückt wird, soll die Zeit wieder aktiviert werden
        public void Quit()
        {
            Application.Quit();
        }
    }
}
