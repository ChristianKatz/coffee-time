﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class PlayerMovement : MonoBehaviour
    {
        //Int Wert für die Spieler Laufgeschwindigkeit
        public int runSpeed;

        //Int Wert für die Sprungkraft
        public int jumpForce;

        //Float Wert für die X-Achsen Werte, die von -1 bis 1 gezählt werden können
        private float moveHorizontal;

        //Int Wert, um die Anzahl der übrigen extra Sprünge anzugeben
        private int extraJump;

        //Rigidbody für die Beeinflussung der Spieler Physik
        private Rigidbody2D rb;
        public Rigidbody2D RB
        {
            get
            {
                return rb;
            }
            set
            {
                rb = value;
            }
        }

        //Bool Wert, um zu sehen, ob der Spieler sich nach rechts oder links bewegt
        private bool facingRight = true;

        //GameObjekt, die das Menü aufruft, wenn der Spieler stirbt
        [SerializeField]
        private GameObject DeadMenu;

        //Bool Wert, der zeigt, ob der Spieler am Boden ist oder nicht
        private bool isGrounded;

        //Transform, der die Lokation angibt, ob der Spieler am Boden ist oder nicht
        [SerializeField]
        private Transform groundCheck;

        //Float Wert, der den Radius für das Abfragen des Bodens angibt
        [SerializeField]
        private float checkRadius;

        //LayerMask, die angibt auf welchen Layer der Boden überprüft werden soll
        [SerializeField]
        private LayerMask WhatIsGrounded;

        //Animtaor, um die Animationen ausführen zu können
        [SerializeField]
        private Animator animator;

        //Int Wert, der die Leben bestimmt
        private int health = 4;
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                health = value;
            }
        }

        void Start()
        {
            //Aufrufen des Rigidbodys
            rb = GetComponent<Rigidbody2D>();

            //Damit die Zeit auch sicherlich zurückgesetzt wurde, wenn das Spiel gestartet wird
            Time.timeScale = 1;

            //Wenn das Spiel gestartet wird, soll das "DeadMenu", bis der Spieler stirbt, geschlossen seien
            DeadMenu.SetActive(false);

            //Cursor ist gelockt und unsichtbar, da es für das Gameplay irrelevant ist
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        void Update()
        {        
            //Die Laufanimation wird bei einem Wert über 0 ausgeführt
            animator.SetFloat("speed", Mathf.Abs(moveHorizontal));

            //Es wird kontrolliert, ob der Spieler auf den Boden ist oder nicht
            isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, WhatIsGrounded);

            //Wenn der Spieler auf den Boden ist, kann er insgesamt 2 mal springen
            if (isGrounded == true)
            {
                extraJump = 1;

                //Wenn der Spieler W drückt soll er springen können
                //Durch den Bool Wert soll in FixedUpdate der Rigidbody2D nach oben bewegt werden
                //Weil er nur zweimal hintereinander springen darf soll der extraJump Wert um 1 reduziert werden
                //Wenn er springt soll die Animation für das Springen aktiviert werden
                if (Input.GetKeyDown(KeyCode.W) && extraJump > 0)
                {
                    animator.SetBool("IsJumping", true);                    
                    extraJump--;
                    rb.velocity = Vector2.up * jumpForce;
                }
                else
                {
                    animator.SetBool("IsJumping", false);
                    
                }
            }

            else
            {
                if (Input.GetKeyDown(KeyCode.W) && extraJump > 0)
                {
                    animator.SetBool("IsJumping", true);                    
                    extraJump--;
                    rb.velocity = Vector2.up * jumpForce;
                }
            }

            //Wenn der Spieler 0 Leben erreicht, bleiben die Leben auf null und der Spieler wird Bewegungsunfähig
            //Dabei öffnet sich das Menü
            if (Health <= 0)
            {
                Health = 0;
                DeadMenu.SetActive(true);
            }

            //Der Spieler darf nicht mehr als 4 Leben haben
            //Falls er darüber hinaus gehen sollte bleibt der Wert 4      
            if (Health > 4)
            {
                Health = 4;
            }

        }

        private void FixedUpdate()
        {
            //Der Spieler soll sich mit der A und D Taste auf der X-Achse bewegen können
            //Dabei wird der Rigidbody bewegt, deshalb ist auch FixedUpdate nötig
            moveHorizontal = Input.GetAxis("Horizontal");
            rb.velocity = new Vector2(moveHorizontal * runSpeed, rb.velocity.y);

            //Wenn der Spieler sich nach links bewegt und der Wert von "moveHorizontal" höher als 0 ist, soll der Sprite geflipt werden
            //Somit guckt der Charakter in die richtige Bewegungsrichtung
            if (facingRight == false && moveHorizontal > 0)
            {
                Flip();
            }

            //Wenn der Spieler sich nach rechts bewegt und der Wert von "moveHorizontal" kleiner als 0 ist, soll der Sprite geflipt werden
            //Somit guckt der Charakter in die richtige Bewegungsrichtung
            if (facingRight == true && moveHorizontal < 0)
            {
                Flip();
            }
        }

        //Methode, die den Sprite des Spielers flipt, wenn er sich nach rechts bzw. links bewegt
        void Flip()
        {
            facingRight = !facingRight;
            Vector2 Scaler = transform.localScale;
            Scaler.x *= -1;
            transform.localScale = Scaler;
        }
    }
}
