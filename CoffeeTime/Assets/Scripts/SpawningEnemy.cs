﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class SpawningEnemy : MonoBehaviour
    {
        //Für die spawnenen Wassertropfen brauche ich die 3 unterschiedlichen Wassertropfen und die Location, wo sie spawnen sollen
        public GameObject[] Enemy;
        public Transform Location;

        //Die Geschwindigkeit der fallenden Wassertropfen
        public int speed;

        //GameObjekt für das Spawnene Objekt sowie Location
        //Die Property ist für das Nutzen der Variable für einen anderen Skript notwenig, wenn der Acces Modifier Private ist
        private GameObject spawnedObject;
        public GameObject SpawnedObject
        {
            get
            {
                return spawnedObject;
            }
            set
            {
                spawnedObject = value;
            }
        }

        // Int Wert, der dafür sorgt, dass die Regentropfen zerstört werden, wenn sie den Spieler nicht treffen und außerhalb der Karte fallen
        public int Destructionvalue;

        void Update()
        {
            //Die Wassertropfen kriegen entlang der X-Achse eine Random Spawning Position
            Enemy[Random.Range(0, 2)].transform.position = new Vector2(Random.Range(-4, 4), transform.position.y);

            //Wenn kein Objekt gespawnt wurde, wird eins von drei verschiedenen entlang der X-Achse gespawnt
            if (spawnedObject == null)
            {
                spawnedObject = Instantiate(Enemy[Random.Range(0, 2)], Location);
            }

            //Falls der Regentropfen den Spieler nicht trifft wird er bei einem bestimmten Y-Achsen Wert zerstört
            //Danach wird ein neuer Regentropfen gespawnt
            if (Destructionvalue > spawnedObject.transform.position.y)
            {
                Destroy(spawnedObject);
                spawnedObject = Instantiate(Enemy[Random.Range(0, 2)], Location);
            }

            //Wenn ein Objekt gespawnt wurde, wird es nach unten beschleunigt
            spawnedObject.transform.Translate(new Vector2(0, -speed * Time.deltaTime));
        }

    }
}

