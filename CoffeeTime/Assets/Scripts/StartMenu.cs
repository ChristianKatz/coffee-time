﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class StartMenu : MonoBehaviour
    {
        //Szenenamen für den Spiel Start, Highscore und der Level Auswahl
        [SerializeField]
        private string SceneNameForPlay;
        [SerializeField]
        private string SceneNameForFirstLevel;
        [SerializeField]
        private string SceneNameForSecondLevel;
        [SerializeField]
        private string SceneNameForThirdLevel;
        [SerializeField]
        private string ScenenameForHighscoreMenu;
        [SerializeField]
        private string ScenenameForBack;

        //GameObjekt, das die Level Selection beinhaltet
        [SerializeField]
        private GameObject activateLevelSelection;

        //GameObjekt, das die erste Seite offline stellt, wenn man in die Level Auswahl gehen will
        [SerializeField]
        private GameObject SetFirstPageOff;


        private void Start()
        {
            //Beim Start des Menüs wird die Level Auswahl offline gesetzt
            activateLevelSelection.SetActive(false);
        }

        //Methode für einen Button, der das Spiel startet
        public void Play()
        {
            SceneManager.LoadScene(SceneNameForPlay);
        }

        //Methode für einen Button, der das Spiel beenden lässt
        public void Quit()
        {
            Application.Quit();
        }

        //Methode für einen Button, der die erste Menü Seite ausschaltet und die zweite mit der Level Auswahl aktiviert
        public void LevelSelection()
        {
            activateLevelSelection.SetActive(true);
            SetFirstPageOff.SetActive(false);
        }

        //Mehtoden, für die Button, die die entsprechenden 3 Level startet
        public void FirstLevel()
        {
            SceneManager.LoadScene(SceneNameForFirstLevel);
        }
        public void SecondLevel()
        {
            SceneManager.LoadScene(SceneNameForSecondLevel);
        }
        public void ThirdLevel()
        {
            SceneManager.LoadScene(SceneNameForThirdLevel);
        }

        //Methode, für einen Button, der das Menü für den Highscore aufruft
        public void HighscoreMenu()
        {
            SceneManager.LoadScene(ScenenameForHighscoreMenu);
        }

        //Methode, für einen Button, der von der Level Auswahl zurück zur ersten Menü Seite führt
        public void Back()
        {
            SceneManager.LoadScene(ScenenameForBack);
        }
    }
}
