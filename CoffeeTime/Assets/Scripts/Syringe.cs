﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Syringe : MonoBehaviour
    {
        //Um die Spritze wieder zu zerstören, brauche ich das Objekt
        [SerializeField]
        private GameObject Boost;

        //Um die Geschwindigkeit des Spielers beeinflussen zu können, brauche ich den Skript "PlayerMovement"
        private PlayerMovement Player;

        //Deaktivierung des Sprite Renderers und des Colliders nach dem Aufsammeln 
        //Weil das Objekt soll durch die Coroutine nicht direkt zerstört werden und andernseits darf der Spieler es nicht merfach aufsammeln
        private SpriteRenderer SyringeSprite;
        private Collider2D SyringeCollider;

        [SerializeField]
        private GameObject HighFace;
        [SerializeField]
        private GameObject NormalFace;

        private void Start()
        {
            //Skripte und Komponenten werden aufgerufen
            Player = FindObjectOfType<PlayerMovement>();
            SyringeSprite = GetComponent<SpriteRenderer>();
            SyringeCollider = GetComponent<Collider2D>();
        }

        //Wenn der Spieler den Collider berührt wird er gestärkt, Collider und Sprite Renderer deaktiviert und die Coroutine gestartet 
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                Player.runSpeed = 15;
                Player.jumpForce = 14;
                SyringeSprite.enabled = false;
                SyringeCollider.enabled = false;
                HighFace.SetActive(true);
                NormalFace.SetActive(false);
                StartCoroutine(deactivateBoost());
            }
        }

        //Cooldown für die Spritze, danach kriegt der Spieler seine Standard Werte zurück und die Spritze wird vom Spiel entfernt
        IEnumerator deactivateBoost()
        {
            yield return new WaitForSeconds(10f);
            Player.runSpeed = 10;
            Player.jumpForce = 12;
            HighFace.SetActive(false);
            NormalFace.SetActive(true);
            Destroy(Boost);
        }
    }
}
