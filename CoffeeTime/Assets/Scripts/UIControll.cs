﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace CoATwoLA
{
    public class UIControll : MonoBehaviour
    {
        //Text Variablen, um die Zeit, den Score und die Leben anzeigen zu lassen
        [SerializeField]
        private TextMeshProUGUI TimeText;
        [SerializeField]
        private TextMeshProUGUI HealthText;
        [SerializeField]
        private TextMeshProUGUI ScoreText;

        //Ich brauche den Skript, um die aktuellen Leben des Spielers anzeigen zu lassen
        private PlayerMovement playerMovement;

        //Float Wert, um die Zeit in Sekunden anzeigen zu lassen
        private float currentTime;

        //Variable, um den aktuellen Score zu zählen
        //Property, um es in einem anderen Skript nutzen zu können, weil der Score sich durch das Einsammeln der Kaffeebohnen ändert
        private int currentScore;
        public int CurrentScore
        {
            get
            {
                return currentScore;
            }
            set
            {
                currentScore = value;
            }
        }

        void Start()
        {
            //Den Skript aufrufen
            playerMovement = FindObjectOfType<PlayerMovement>();
        }

        void Update()
        {
            //Die Zeit soll im Spiel aktuell in Sekunden angezeigt werden
            currentTime = Time.timeSinceLevelLoad;
            TimeText.text = string.Format("Time: {0:0}", currentTime);

            //Aktuelle Anzeige der Leben und des Scores
            ScoreText.text = string.Format("Score: {0:0}", currentScore);
            HealthText.text = string.Format(playerMovement.Health + " x");

            //Speicherung des Scores und der Zeit für den Highscore
            PlayerPrefs.SetInt("Coffeebeans", CurrentScore);
            PlayerPrefs.SetFloat("Time", currentTime);
            PlayerPrefs.Save();

        }
    }
}
