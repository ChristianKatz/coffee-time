﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class Umbrella : MonoBehaviour
    {
        //Ich brauche das GameObjekt, damit er sich nach dem Drücken der F Taste aktivieren kann
        public GameObject Shield;

        //Das GameObjekt "PressF" ist für die Anzeige notwendig, dass der Regenschirm verwendet werden kann
        [SerializeField]
        private GameObject PressF;

        //Der Skript "DestroyUmbrella" ist für die Bestätigung, dass der Spieler den Regenschirm eingesammelt hat und somit aktiviert werden kann
        private DestroyUmbrellaSprite DestroyUmbrella;

        void Start()
        {
            //Den Skript aufrufen
            DestroyUmbrella = FindObjectOfType<DestroyUmbrellaSprite>();

            //Da der Standard Bool Wert true ist, muss ich ihn auf false setzten, weil der Regenschirm nicht nach dem Start des Spieles aktiviert werden soll
            PressF.SetActive(false);
            Shield.SetActive(false);

        }
        private void Update()
        {
            //Wenn der Spieler den Regenschirm eingesammelt hat, sieht er, dass er ihn einsetzten kann
            //Nachdem er die F Taste gedrückt hat, wird der Regenschirm aktiviert und die F Taste wieder deaktiviert
            if (DestroyUmbrella.DestroyedSprite == true)
            {
                PressF.SetActive(true);
                if (Input.GetKey(KeyCode.F))
                {
                    Shield.SetActive(true);
                    PressF.SetActive(false);
                    DestroyUmbrella.DestroyedSprite = false;
                }
            }
        }
    }
}

