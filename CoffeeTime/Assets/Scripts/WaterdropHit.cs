﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoLA
{
    public class WaterdropHit : MonoBehaviour
    {
        //Brauche den Skript vom Spieler, um die Leben bei einer Berührung abzuziehen
        private PlayerMovement Player;

        //Brauche den Skript von den spawnenen Wassertropfen, um bei einer Berührung einen neuen Wassertropfen spawnen zu lassen
        private SpawningEnemy spawning;

        void Start()
        {
            //Beide Scripte werden aufgerufen
            Player = FindObjectOfType<PlayerMovement>();
            spawning = GetComponentInParent<SpawningEnemy>();
        }

        //Trigger setzen, damit der Spieler bei einer Berührung Leben abgezogen bekommt
        //Falls der Spieler ein Regentropfen berührt wird das GameObject zerstört und ein neues wird gespawnt
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Destroy(spawning.SpawnedObject);
                Player.Health--;
                spawning.SpawnedObject = Instantiate(spawning.Enemy[Random.Range(0, 2)], spawning.Location);
            }
        }
    }
}
