﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoLA
{
    public class YouAreDeadMenu : MonoBehaviour
    {
        //String Wert für den Szenenamen
        [SerializeField]
        private string ScenenameForTheMenu;

        private void Update()
        {
            //Wenn das Menü aufgerufen wird, soll das Spiel stoppen, um den Zeit Highscore nicht zu verfälschen
            Time.timeScale = 0;

            //Wenn das Menü aufgerufen wird, soll der Spieler den Cursor sehen und bewegen können
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        //Methode für einen Button, um zurück ins Menu zu kommen
        //Wenn der Button gedrückt wird, soll die Zeit wieder aktiviert werden
        public void Menu()
        {
            SceneManager.LoadScene(ScenenameForTheMenu);
            Time.timeScale = 1;
        }

        //Methode für ein Button, um das Level nochmal zu versuchen
        //Wenn der Button gedrückt wird, soll die Zeit wieder aktiviert werden
        public void TryAgain()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Time.timeScale = 1;
        }
    }
}
